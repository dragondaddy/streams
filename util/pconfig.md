CLI pconfig utility
==================

Usage:


pconfig
	displays all available channels and their ids

pconfig {{uid}}
	displays all pconfig entries for {{uid}}

pconfig {{uid}} family
	displays all pconfig entries for family (system, database, etc)

pconfig {{uid}} family key
	displays single pconfig entry for specified family and key

pconfig {{uid}} family key value
	set pconfig entry for specified family and key to value and display result

NOTES:
	family and key may be entered in dot notation, example:
		pconfig 14 system.activitypub_enabled
	is the  same as
		pconfig 14 system activitypub_enabled
	
