Le canal Système
================

Cette page décrit un projet encore en cours d'élaboration. Les fonctionnalités décrites ici sont en cours de développement, d'autres fonctionnalités seront ajoutées à l'avenir ainsi que de la documentation supplémentaire.


Chaque site/instance dispose d'un "canal système" dont l'URL se trouve à la racine du site/domaine. L'administration de ce canal est assurée par l'administrateur du site. Il se présente comme un canal, tout comme n'importe quel autre canal de réseau social, mais son comportement est légèrement différent, puisqu'il représente l'ensemble du site. L'administration s'effectue à partir du lien "Canaux" lorsque vous cliquez sur votre photo de profil en haut de la page. Si vous êtes l'administrateur, vous verrez que vous pouvez "basculer" vers ce canal.

## Définir votre identité/marque

La modification du nom du canal Système modifie le nom du site. Cette opération peut être effectuée dans les paramètres du canal ou en modifiant le profil.

La modification de la photo de profil du canal système change l'icône ou l'avatar du site.

Le type du canal peut être modifié pour passer du type "Social - Normal" à un des types de groupe disponibles, si vous le souhaitez pour les besoins de votre site. Cette utilisation n'a fait l'objet que de très peu de tests jusqu'à présent. Veuillez ne pas modifier ce paramètre, à moins que vous ne souhaitiez aider à tester cette fonctionnalité.

## Connexions

D'autres personnes peuvent se connecter au canal Système comme s'il s'agissait d'un membre normal du site. Cela peut nécessiter une approbation. Les personnes qui suivent ce canal verront les publications/conversations publiques de tous les membres du site.

Le canal Système peut se connecter à d'autres sites. Si vous suivez un canal Système d'un autre site, le contenu public de ce site sera intégré au flux public local (si le flux public est activé).

## Communautés

Si deux sites se connectent l'un à l'autre **et** ont la même marque ou le même nom de site, ils apparaîtront dans la communauté correspondante de l'application Communautés. Les membres de ces sites seront connectés en tant que membres d'un même "super-site" partageant la plupart du temps le même contenu public.
