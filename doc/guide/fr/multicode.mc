Multicode
=========


Le multicode est une fonctionnalité unique de cette gamme d'applications de communication. Il permet de créer facilement du contenu en texte enrichi et/ou du contenu multimédia sans se préoccuper du format sous-jacent. Le contenu peut être créé en utilisant du HTML, Markdown, ou BBcode - ou tout cela à la fois.

[code]
<b>Ceci devrait apparaître en gras.</b> (HTML < 5)
<strong>Ceci aussi devrait.</strong> (HTML5)
**Tout comme ceci.** (Markdown)
[b]Ou encore ceci.[/b] (BBcode)
[/code]

<b>Ceci devrait apparaître en gras.</b> (HTML < 5)
<strong>Ceci aussi devrait.</strong> (HTML5)
**Tout comme ceci.** (Markdown)
[b]Ou encore ceci.[/b] (BBcode)

À propos de cette implémentation:

Pour éviter les mauvaises surprises, veillez à utiliser la fonction d'aperçu lorsque vous rédigez quelque chose de complexe. Pour prévisualiser votre message sans le publier, cliquez sur l'icône "œil" située sous l'éditeur de publication ou de commentaire. Un aperçu du message s'affichera juste en dessous de la zone d'édition. Cliquez à nouveau sur l'icône "œil" pour mettre à jour l'aperçu à tout moment. Un aperçu est automatiquement généré lorsque du contenu multimédia est ajouté via le bouton "Joindre/intégrer un fichier" ("Attach/Embed file") ou lors de l'insertion de liens vers des pages web via l'outil d'ajout de lien.  

Le format Markdown est très sensible aux "faux positifs" qui peuvent ajouter des instructions de balisage de manière non intentionnelle lorsque des caractères de ponctuation sont utilisés de manière inhabituelle ou peu courante. Cela peut se produire (par exemple) si vous incluez du code informatique dans un message sans l'identifier comme un bloc de code. Si une séquence markdown n'est pas interprétée correctement, essayez d'ajouter des espaces autour de la ponctuation déclenchante et assurez-vous que tout le code informatique est placé dans des blocs de code.

Markdown propose deux méthodes pour créer des blocs de code. L'une d'entre elles consiste à entourer le bloc de code de trois apostrophes inversées (```) en début de ligne avant et après le code. La seconde consiste à indenter le texte de 4 espaces ou d'une largeur de tabulation. Cette seconde forme provoque de nombreux faux positifs dans des textes par ailleurs normaux et, pour cette raison, n'est pas prise en charge ici.

Le code HTML doit être fortement filtré dans les applications web multi-utilisateurs afin de prévenir et d'éviter un comportement nuisible appelé "Cross-site scripting". Vous ne pouvez pas du tout utiliser Javascript, et il existe un certain nombre de restrictions concernant les éléments rich-media tels que les balises audio/vidéo et les contenus inter-domaines tels que les iframes. Pour nombre de ces éléments, nous utilisons le balisage BBcode localement, comme vous pourrez le constater si vous incluez une vidéo ou un lien avec du contenu intégré. Vous pourrez vous en inspirer pour procéder de la même manière. 

Sachez également que tous les sauts de ligne dans vos messages sont normalement conservés, ce qui peut affecter l'affichage des listes et des tableaux HTML. Lorsque vous utilisez le langage HTML pour créer de tels éléments, vous pouvez être tenté de leur donner un aspect "soigné" en plaçant chaque élément de tableau ou de liste sur sa propre ligne. Cela ne s'affichera généralement pas correctement et pourrait inclure un certain nombre de lignes vides superflues, en raison des sauts de ligne conservés. Pour obtenir les meilleurs résultats lors de la création de listes et de tableaux en HTML, chaque élément de liste ou rangée de tableau HTML doit être accolé à celui qui le précède sans qu'aucun saut de ligne ne soit effectué entre eux. 
