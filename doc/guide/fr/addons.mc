Addons
======

Les addons fournissent des fonctionnalités supplémentaires que les auteurs originaux du logiciel n'ont pas implémentées ou n'avaient peut-être pas imaginées. Ils doivent être installés par l'administrateur du site avant que vous ne puissiez les utiliser. 

### ABC Music

Rendu de la notation musicale ABC sous forme de partitions musicales

### Content Importer

Importer le contenu et les fichiers d'un canal dans un canal cloné

### Dreamhost

Amélioration du fonctionnement sur l'hébergement mutualisé Dreamhost

### Faces

Détection des visages dans les images, identification et reconnaissance faciale

### Flashcards

Méthode d'apprentissage à l'ancienne qui utilise la répétition espacée comme technique d'apprentissage

### Followlist

Suivre tout le monde dans une liste followers/following ActivityPub ou dans un fichier d'export de contacts de Mastodon

### Fuzzloc

Si vous utilisez les services de localisation de votre navigateur pour indiquer l'emplacement de votre publication, cet addon vous permet de masquer l'emplacement exact et de le remplacer par une indication approximative

### Hexit

Outil de conversion en hexadécimal

### LDAP Authenticate

Authentification de compte auprès d'un annuaire LDAP d'entreprise tel que Windows Active Directory

### Logrot

Rotation des journaux (logs)

### Not Safe For Work

Masquer les messages au contenu inapproprié (contenant la chaîne de caractères "nsfw")

### Open Search

Configurez votre site en tant que fournisseur open search, ce qui vous permet de l'ajouter en tant que moteur de recherche dans votre navigateur web

### OpenStreetMap

Utiliser OpenStreetMap pour afficher les lieux

### phpmailer

Utiliser phpmailer au lieu de la fonction mail() intégrée pour envoyer des notifications par courrier électronique. Cela permet de disposer de nombreuses options de configuration pour travailler avec des environnements de messagerie complexes.

### Qrator

Générateur de QR code

### Queue Worker

Gestionnaire de file d'attente de nouvelle génération pour les tâches en arrière-plan (BETA)

### Rainbowtag

Ajoutez de la couleur aux nuages de tags

### Standard Embeds

Permettre un accès non filtré aux contenus intégrés issus des principaux fournisseurs de médias (actuellement YouTube, Soundcloud et Twitter)

### Stream Order

Fournit un widget permettant de modifier l'ordre des publications

### Sudo

Permettre à l'administrateur du site d'accéder aux canaux et contenus protégés et de les administrer

### Twitter API

Implémente une partie de l'API Twitter V1 et V1.1 avec des extensions StatusNet
