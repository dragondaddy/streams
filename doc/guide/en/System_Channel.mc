The System Channel
==================

This page describes a work in progress. These features are under development and additional functionality and documentation will be provided in the future.


Every site/instance has a 'system channel' with a URL at the site/domain root. Administration of this channel is performed by the site administrator. It appears as a channel, just like any other social channel but behaves slightly different - as it represents the entire site. Administration is performed from the 'Channels' link when you click on your profile photo at the top of the page. If you are the administrator, you will see this channel as one you can "switch" to.

## Setting your brand

Changing the channel name of the system channel changes the name of the site. This can be accomplished in the channel settings or by editing the profile.

Changing the profile photo of the system channel changes the site icon or avatar.

The channel role can be changed from social-normal to a group if desired for your site requirements. This usage has had only very light testing to date. Please leave that setting alone unless you are willing to help test this feature.

## Connections

Other people can connect with the system channel as if it were a normal site member. This may require approval. Followers of this channel will see the public posts/conversations of all site members.

The system channel can connect with others. If following a system channel from another site, this will pull that site's public content into the local public stream (if the public stream is enabled).

## Communities

If two sites connect with each other **and** have the same brand or site name, they will be shown in the appropriate community in the Communities app. Members of these sites will be connected as a "super-site" sharing much of the same public content.




