Service Classes
===============

Service classes allow you to set limits on system resources by limiting what individual accounts can do, including file storage and top-level post limits. Define custom service classes according to your needs in the `.htconfig.php` file. For example, create a _standard_ and _premium_ class using the following lines:

```
// Service classes

App::$config['system']['default_service_class']='standard'; // this is the default service class that is attached to every new account

// configuration for standard service class
App::$config['service_class']['standard'] =
array('photo_upload_limit'=>2097152, // total photo storage limit per channel (here 2MB)
'total_identities' =>1, // number of channels an account can create
'total_items' =>0, // number of top level posts a channel can create (other posts and comments are unaffected)
'total_channels' =>100, // number of connections the user can add (other users can still add this channel, even if the limit is reached)
'attach_upload_limit' =>2097152); // total attachment storage limit per channel (here 2MB)

// configuration for premium service class
App::$config['service_class']['premium'] =
array('photo_upload_limit'=>20000000000, // total photo storage limit per channel (here 20GB)
'total_identities' =>20, // number of channels an account can create
'total_items' =>20000, // number of top level posts a channel can create (other posts and comments are unaffected)
'total_channels' =>2000, // number of connections the user can add (other users can still add this channel, even if the limit is reached)
'attach_upload_limit' =>20000000000); // total attachment storage limit per channel (here 20GB)
```

To apply a service class to an existing account, use the command line utility from the web root:

`util/service_class`
list service classes

`util/config system default_service_class firstclass`
set the default service class to 'firstclass'

`util/service_class firstclass`
list the services that are part of 'firstclass' service class

`util/service_class firstclass photo_upload_limit 10000000`
set firstclass total photo disk usage to 10 million bytes

`util/service_class --account=5 firstclass`
set account id 5 to service class 'firstclass' (with confirmation)

`util/service_class --channel=blogchan firstclass`
set the account that owns channel 'blogchan' to service class 'firstclass' (with confirmation)

**Service class limit options**

* photo_upload_limit - maximum total bytes for photos
* total_items - maximum total toplevel posts
* total_identities - maximum number of channels owned by account
* total_channels - maximum number of connections
* attach_upload_limit - maximum file upload storage (bytes)
* access_tokens - maximum number of Guest Access Tokens per channel

