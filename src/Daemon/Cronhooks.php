<?php

/** @file */

namespace Code\Daemon;
use Code\Extend\Hook;
use Code\Lib\Time;

class Cronhooks implements DaemonInterface
{

    public function run(int $argc, array $argv): void
    {

        logger('cronhooks: start');

        $d = Time::convert();

        Hook::call('cron', $d);
    }
}
