<?php

namespace Code\Update;

use Code\Access\PermissionRoles;
use Code\Lib\PConfig;
use Code\Lib\AbConfig;

class _1274
{

    public function run()
    {

        $r = q("select channel_id from channel where true");
        if ($r) {
            foreach ($r as $rv) {
                PConfig::Set($rv['channel_id'], 'perm_limits', 'hyperdrive', PERMS_SPECIFIC);
                $role = PConfig::Get($rv['channel_id'], 'system', 'permissions_role');
                if (str_contains($role, 'social')) {
                    $abooks = q("select * from abook where abook_pending = 0 and abook_self = 0 and abook_channel = %d",
                        intval($rv['channel_id'])
                    );
                    if ($abooks) {
                        foreach ($abooks as $abook) {
                            $perms = AbConfig::Get($rv['channel_id'], $abook['abook_xchan'], 'system', 'my_perms', '' );
                            $s = explode(',', $perms);
                            $s[] = 'hyperdrive';
                            AbConfig::Set($rv['channel_id'], $abook['abook_xchan'], 'system', 'my_perms', implode(',', $s));
                        }
                    }
                }
            }
        }
        return UPDATE_SUCCESS;
    }

    public function verify() {
        return true;
    }

}
