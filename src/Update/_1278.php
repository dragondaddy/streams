<?php

namespace Code\Update;

class _1278
{
    public function run()
    {

        q("START TRANSACTION");

        if (ACTIVE_DBTYPE == DBTYPE_POSTGRES) {
            $r1 = q("ALTER TABLE item ALTER COLUMN lat TYPE double precision");
            $r2 = q("ALTER TABLE item ALTER COLUMN lon TYPE double precision");
            $r = ($r1 && $r2);
        }
        else {
            $r = q("ALTER TABLE `item` 
                CHANGE `lat` `lat` DOUBLE NOT NULL DEFAULT '0', 
                CHANGE `lon` `lon` DOUBLE NOT NULL DEFAULT '0'"
            );
        }

        if ($r) {
            q("COMMIT");
            return UPDATE_SUCCESS;
        }

        q("ROLLBACK");
        return UPDATE_FAILED;
    }

    public function verify()
    {

        $columns = db_columns('item');

        if (in_array('lat', $columns) && in_array('lon', $columns)) {
            return true;
        }
        return false;
    }





}