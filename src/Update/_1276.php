<?php

namespace Code\Update;

use Code\Lib\Channel;
use Code\Lib\Multibase;

class _1276
{
    public function run()
    {
        $channels = q("select * from channel where true");
        if ($channels) {
            foreach ($channels as $channel) {
                $epubkey = (new Multibase())->publicKey($channel['channel_epubkey']);
                q("update xchan set xchan_epubkey = '%s' where xchan_url = '%s'",
                    dbesc($epubkey),
                    dbesc(Channel::url($channel))
                );
            }
        }
        return UPDATE_SUCCESS;
    }

    public function verify()
    {
        return true;
    }
}