<?php

namespace Code\Module;

use Code\Web\Controller;

class Pushsub extends Controller
{

    public function init()
    {

        $rawSubscription = file_get_contents('php://input');
        $channel_id = local_channel();

        $subscription = json_decode($rawSubscription, true);

        if (!isset($subscription['endpoint'])) {
            logger('not a subscription');
            killme();
        }

        $method = $_SERVER['REQUEST_METHOD'];

        switch ($method) {
            case 'POST':
                $result = q("insert into pushsub (channel_id, endpoint, json) values ('%s', '%s', '%s')",
                    intval($channel_id),
                    dbesc($subscription['endpoint']),
                    dbesc($rawSubscription)
                );
                break;
            case 'PUT':
                $result = q("update pushsub set json = '%s' where endpoint = '%s' and channel_id = %d",
                    dbesc($rawSubscription),
                    dbesc($subscription['endpoint']),
                    intval($channel_id)
                );
                break;
            case 'DELETE':
                $result = q("delete pushsub where endpoint = '%s' and channel_id = %d",
                    dbesc($subscription['endpoint']),
                    intval(local_channel())
                );
                break;
            default:
                logger('method not handled');
                break;
        }
        killme();
    }

}