<?php

set_include_path(
    '../include' . PATH_SEPARATOR
    . '../library' . PATH_SEPARATOR
    . '../' . PATH_SEPARATOR . './'
);
define('\UNIT_TESTING', 1);
require_once('boot.php');
