<?php
namespace Code\Tests\Unit\Lib;

use Code\Lib\ActorId;
use Code\Tests\Unit\UnitTestCase;

class ActorIdTest extends UnitTestCase
{

    public function testActorId()
    {
        $this->assertEquals('did:ap:key:abc12345', (new ActorId('https://resolver.com/resolver/did:ap:key:abc12345?foo'))->getId());
        $this->assertEquals('did:ap:key:abc12345', (new ActorId('https://resolver.com/resolver/did:ap:key:abc12345#foo'))->getId());
        $this->assertEquals('https://example.com/actor', (new ActorId('https://example.com/actor'))->getId());
        $this->assertEquals('did:ap:key:abc12345',  (new ActorId('did:ap:key:abc12345'))->getId());
    }



}
